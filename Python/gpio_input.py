import RPi.GPIO as GPIO ## Import GPIO library
import time
led1 = 5
led2 = 6
led3 = 12
led4 = 13


GPIO.setmode(GPIO.BCM) ## Use GPIO pin numbering
GPIO.setup(led1, GPIO.IN) ## Setup GPIO Pin 7 to OUT
GPIO.setup(led2, GPIO.IN)
GPIO.setup(led3, GPIO.IN)
GPIO.setup(led4, GPIO.IN)

while True:
    print "input 1: ", GPIO.input(led1)
    print "input 2: ", GPIO.input(led2)    
    print "input 3: ", GPIO.input(led3)
    print "input 4: ", GPIO.input(led4)
    time.sleep(1)

GPIO.cleanup()

