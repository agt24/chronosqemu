#!/usr/bin/python

import os

def getDiskSpace():
    p=os.popen("df /")
    line = p.readline()
    line = p.readline()
    return(line.split()[1:4])

a = getDiskSpace()
print int((float(a[1])/float(a[0]))*100)
