import RPi.GPIO as GPIO ## Import GPIO library
import time
led1 = 5
led2 = 6
led3 = 12
led4 = 13
led5 = 20
led6 = 26
led7 = 16
led8 = 19
pulserPin = 17

GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM) ## Use GPIO pin numbering
GPIO.setup(led1, GPIO.OUT) ## Setup GPIO Pin 7 to OUT
GPIO.setup(led2, GPIO.OUT)
GPIO.setup(led3, GPIO.OUT)
GPIO.setup(led4, GPIO.OUT)
GPIO.setup(led5, GPIO.OUT)
GPIO.setup(led6, GPIO.OUT)
GPIO.setup(led7, GPIO.OUT)
GPIO.setup(led8, GPIO.OUT)
GPIO.setup(led8, GPIO.OUT)
GPIO.setup(pulserPin, GPIO.OUT)

p = GPIO.PWM(pulserPin, 50000)
p.start(50)

while True:
    GPIO.output(led1,True)
    GPIO.output(led2,True)    
    GPIO.output(led3,True)
    GPIO.output(led4,True)
    GPIO.output(led5,True)
    GPIO.output(led6,True)    
    GPIO.output(led7,True)
    GPIO.output(led8,True)
    print "ON"
    time.sleep(2)
    GPIO.output(led1,False)
    GPIO.output(led2,False)
    GPIO.output(led3,False)
    GPIO.output(led4,False)
    GPIO.output(led5,False)
    GPIO.output(led6,False)
    GPIO.output(led7,False)
    GPIO.output(led8,False)
    print "OFF"
    time.sleep(1)

GPIO.cleanup()
