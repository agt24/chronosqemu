#!/usr/bin/python

import os
import glob
import MySQLdb
import RPi.GPIO as GPIO
import ctypes
import commands
import urllib2
import string
import sre
import time

print "Assigning roles to temperature sensors..."
time.sleep(1)

count = 0

while (count == 0):    
    print "Please insert only the temperature sensor used at the Water Outlet. Press any key to continue."
    blah = raw_input('')
    sensorFile1 = open("/sys/bus/w1/devices/w1_bus_master1/w1_master_slave_count","r")
    count = sensorFile1.readline()
    sensorFile1.close()
    if count == 2:
        print "Detected two temperature sensors connected..."
    elif count == 0:
        print "No temperature sensors detected..."
    elif count == 1:
        print "Detecting temperature sensor ID..."

sensorFile2 = open("/sys/bus/w1/devices/w1_bus_master1/w1_master_slaves","r")
waterOutSensorID = sensorFile2.readline()
print "sensor ID : ", waterOutSensorID, " - Assigned to Water Outlet."
sensorFile2.close()
count = 0

while (count == 0):    
    print "Please insert only the temperature sensor used at the Water Inlet. Press any key to continue."
    blah = raw_input('')
    sensorFile3 = open("/sys/bus/w1/devices/w1_bus_master1/w1_master_slave_count","r")
    count = sensorFile3.readline()
    sensorFile3.close()
    if count == 2:
        print "Detected two temperature sensors connected..."
    elif count == 0:
        print "No temperature sensors detected..."
    elif count == 1:
        print "Detecting temperature sensor ID..."
    sensorFile2 = open("/sys/bus/w1/devices/w1_bus_master1/w1_master_slaves","r")
    waterInSensorID = sensorFile2.readline()
    sensorFile2.close()
    if (waterInSensorID == waterOutSensorID):
        print "Detected address is same as Outlet sensor."
        count = 0
print "sensor ID : ", waterInSensorID, " - Assigned to Water Inlet."


sensorData = [waterOutSensorID, waterInSensorID]
dataFile = open('/var/www/sensorConf.txt', 'w')
for eachitem in sensorData:
    dataFile.write(eachitem)
dataFile.close()
