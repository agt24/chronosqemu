#!/usr/bin/python

import os
import glob
import MySQLdb
import RPi.GPIO as GPIO
import ctypes
import commands
import urllib2
import string
import sre
import time

sensorInID = '28-00042d4367ff'
sensorOutID = '28-00042c648eff'

while 1:
    error_T1 = 1
    error_T2 = 1
    try:
        for sensors in range (2):
            base_dir = '/sys/bus/w1/devices/'
            device_folder = glob.glob(base_dir + '28*')[sensors]
            device_file = device_folder + '/w1_slave'
            device_file_ID = device_folder + '/name'
            def read_temp_raw():
                f = open(device_file, 'r') or die("error")
                lines = f.readlines()
                f.close()
                return lines

            def read_ID():
                f = open(device_file_ID, 'r')
                lines = f.read(15)
                f.close()
                return lines

            def read_temp():
                lines = read_temp_raw()
                while lines[0].strip()[-3:] != 'YES':
                   time.sleep(0.2)
                   lines = read_temp_raw()
                equals_pos = lines[1].find('t=')
                if equals_pos != -1:
                   temp_string = lines[1][equals_pos+2:]
                   temp = float(temp_string) / 1000.0 # Divide by 1000 for proper decimal point
                   temp = temp * 9.0 / 5.0 + 32.0 # Convert to degF
                   temp = round(temp, 1) # Round temp to 2 decimal points
                   return temp

            if sensors == 0:
                if(read_ID() == sensorOutID):
                   waterOutTemp = read_temp()
                   print "waterOut = ", waterOutTemp
                   error_T2 = 0
                elif(read_ID() == sensorInID):
                   returnTemp = read_temp()
                   print "return = ", returnTemp
                   error_T1 = 0
            if sensors == 1:
                if(read_ID() == sensorOutID):
                   waterOutTemp = read_temp()
                   print "waterOut = ", waterOutTemp
                   error_T2 = 0
                elif(read_ID() == sensorInID):
                   returnTemp = read_temp()
                   print "return = ", returnTemp
                   error_T1 = 0
    except:
        print " "
