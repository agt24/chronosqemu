<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Chronos : Home Page</title>
	
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
      <!-- bootstrap -->
   <link href="css/bootstrap-combined.min.css" rel="stylesheet">
    <script src="js/jquery-2.0.3.min.js"></script> 
  

    <!-- x-editable (bootstrap version) -->
    <link href="css/bootstrap-editable.css" rel="stylesheet"/>
    <script src="js/bootstrap-editable.min.js"></script>
    
    <!-- main.js -->
    <script src="js/main.js"></script><!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <!-- Bootstrap -->
   
	 <link href="css/style.css" rel="stylesheet">
	<link href="css/bootstrap.min.css" rel="stylesheet">
	
	
	<link href="css/jquery.circliful.css" rel="stylesheet" type="text/css" />
	<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" />

	<script src="http://code.jquery.com/jquery-1.10.2.min.js"></script>
	<script src="js/jquery.circliful.min.js"></script>
	
	
	  <link class="include" rel="stylesheet" type="text/css" href="syntaxhighlighter/jquery.jqplot.min.css" />
    <link rel="stylesheet" type="text/css" href="syntaxhighlighter/examples.min.css" />
    <link type="text/css" rel="stylesheet" href="syntaxhighlighter/styles/shCoreDefault.min.css" />
    <link type="text/css" rel="stylesheet" href="syntaxhighlighter/styles/shThemejqPlot.min.css" />
  
	<script>
$( document ).ready(function() {
        $('#myStathalf').circliful();
		$('#myStat').circliful();
		$('#myStathalf2').circliful();
		$('#myStat2').circliful();
    $('#myStat3').circliful();
    $('#myStat4').circliful();
    $('#myStathalf3').circliful();
    });
</script>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
  
	
	<!-- Top Container -->
	
	<div class="container-fluid" style="background-color:#3D7073;">
		<div class="container jumbotron" style="background-color:#3D7073; padding-top:0; padding-bottom:2px; margin-bottom:0;">
			<div class="row" >
				<div class="col-md-3" style="text-align:center; color:#FFFFFF; background-color:#1E3839; padding:10px 10px;">
					<h4><b>CHRONOS</b></h4>
					<h5>SYSTEM-ONLINE</h5><!--Dynamic Content-->
				</div>
				<div class="col-md-6" style="text-align:center; color:#FFFFFF;">
					<h6><a href="#" style="color:#FFFFFF;">About</a> | 
					<a href="#" style="color:#FFFFFF;">Developer Website</a> |
					<a href="#" style="color:#FFFFFF;">Help</a></h6>
					
						<div class="col-md-12"  style="background-color:#1E3839; min-height:50px; padding-top:5px; padding-bottom:5px;">
							<h5>SYSTEM MAP</h5>
						</div>
					
				</div>
				<div class="hidden-xs">
				<div class="col-md-3" style="text-align:center; color:#FFFFFF; text-align:center; color:#FFFFFF; background-color:#1E3839; min-height:84px;">
					
				</div>
				</div>
			</div>
		</div>
	</div>
	<!-- End of Top Container -->
	
	<div class="container-fluid" style="background-color:#3D7073;">
		<div class="container jumbotron" style="background-color:#3D7073; padding-bottom:0; padding-top:2px;">
			<div class="row" >
				<div class="col-md-3" style="text-align:center; color:#FFFFFF; background-color:#224042; padding:0 0;">
					<div style="width:100%; background-color:#FF6600; padding:10px 10px;" >
					<b>System Statistics</b>
					</div>
					<h5>Outside Temperature</h5>
					<h6 id="OutsideTemp">
					<?php
						include('SetConnect.php');
						$sql="SELECT Temp from test where Temp=92";
						$result=mysql_query($sql,$con);
						if($result){
							while($row=mysql_fetch_array($result)){
							echo $row['Temp']; 
							}
						}
					?> degF</h6>		<!--Dynamic Content-->
					<h6><small>wx.thomaslivestock.com</small></h6>
					<table  align=center>
						<tr>
						<td >Water Inlet</td>
						<td width=5% style="border-right:1px solid white;"></td>
						<td>&nbsp;&nbsp;&nbsp;Water Outlet</td><!--Dynamic Content-->
						</tr>
						<tr>
						<td><h6><?php
						include('SetConnect.php');
						$sql="SELECT Temp from test where Temp=92";
						$result=mysql_query($sql,$con);
						if($result){
							while($row=mysql_fetch_array($result)){
							echo $row['Temp']; 
							}
						}
					?> degF</h6></td>
						<td width=5% style="border-right:1px solid white;"></td>
						<td><h6><?php
						include('SetConnect.php');
						$sql="SELECT Temp from test where Temp=92";
						$result=mysql_query($sql,$con);
						if($result){
							while($row=mysql_fetch_array($result)){
							echo $row['Temp']; 
							}
						}
					?> degF</h6></td><!--Dynamic Content-->
						</tr>
						
					</table>
					<table align=center><br/>
						<h5>Activity Stream</h5><br/>
						<tr>
						<td></td>
						<td width=2%></td>
						<td>Date Time&nbsp;&nbsp;</td><!--Dynamic Content-->
						<td width=2% ></td>
						<td>&nbsp;&nbsp;Status</td>
						</tr>
						<tr>
						<td>Boiler</td>
						<td width=2% style="border-left:1px solid white;"></td>
						<td><h6><?php
						include('SetConnect.php');
						$sql="SELECT datetime from test where Temp=92";
						$result=mysql_query($sql,$con);
						if($result){
							while($row=mysql_fetch_array($result)){
							echo $row['datetime']; 
							}
						}
					?> </h6></td><!--Dynamic Content-->
						<td width=2% style="border-right:1px solid white;"></td>
						<td><h6><?php
						include('SetConnect.php');
						$sql="SELECT tesdatao from test where Temp=92";
						$result=mysql_query($sql,$con);
						if($result){
							while($row=mysql_fetch_array($result)){
							echo $row['tesdatao']; 
							}
						}
					?> </h6></td>
						</tr>
						<tr>
						<td>Chiller</td>
						<td width=5% style="border-left:1px solid white;"></td>
						<td><h6><?php
						include('SetConnect.php');
						$sql="SELECT datetime from test where Temp=92";
						$result=mysql_query($sql,$con);
						if($result){
							while($row=mysql_fetch_array($result)){
							echo $row['datetime']; 
							}
						}
					?></h6></td><!--Dynamic Content-->
						<td width=2% style="border-right:1px solid white;"></td>
						<td><?php
						include('SetConnect.php');
						$sql="SELECT tesdatao from test where Temp=92";
						$result=mysql_query($sql,$con);
						if($result){
							while($row=mysql_fetch_array($result)){
							echo $row['tesdatao']; 
							}
						}
					?> </h6></td>
						</tr>
						<tr>
						<td>Chiller 2&nbsp;&nbsp;</td>
						<td width=5% style="border-left:1px solid white;"></td>
						<td><h6><?php
						include('SetConnect.php');
						$sql="SELECT datetime from test where Temp=92";
						$result=mysql_query($sql,$con);
						if($result){
							while($row=mysql_fetch_array($result)){
							echo $row['datetime']; 
							}
						}
					?></h6></td><!--Dynamic Content-->
						<td width=2% style="border-right:1px solid white;"></td>
						<td><?php
						include('SetConnect.php');
						$sql="SELECT tesdatao from test where Temp=92";
						$result=mysql_query($sql,$con);
						if($result){
							while($row=mysql_fetch_array($result)){
							echo $row['tesdatao']; 
							}
						}
					?> </h6></td>
						</tr>
					</table>
					<br/>
					
				</div>
				
				
				<div class="col-md-6" style="text-align:center; color:#FFFFFF; min-height:300px;">
					<div  style="width:100%; padding:10px 10px; background-image:url('images/demo.png'); 	background-repeat: no-repeat; min-height:300px;" >
						<table border="0" >
							<tr height=30px>
								<td width="260px"></td>
								<td></td>
								<td></td>
								<td></td>
							<tr>
							<tr>
								<td align="right"><img src="images/bitMapTemp.png" /></td>
								<td></td>
								<td ></td>
								<td align="right"> <img src="images/bitMapTempTwo.png"/></td>
							<tr>
							<tr>
								<td></td>
								<td align="left"><div><img src="images/boilerOne.png" /></div></td>
								<td></td>
								<td></td>
							<tr>
							<tr>
								<td></td>
								<td align="left">&nbsp;Boiler 1</td>
								<td></td>
								<td></td>
							<tr>
							<tr>
								<td></td>
								<td align="left" width=100px><img src="images/chillerOne.png" /></td>
								<td align="left"><img src="images/chillerTwo.png"/></td>
								<td></td>
							<tr>
							<tr>
								<td></td>
								<td align="left">&nbsp;Chiller 1</td>
								<td align="left">&nbsp;Chiller 2</td>
								<td></td>
							<tr>
						</table>
					</div>
					
				</div>
				
				<div class="col-md-3" style="text-align:center; color:#FFFFFF; background-color:#224042; padding:0 0;">
					<div style="width:100%; background-color:#FF6600; padding:10px 10px;" >
					<b>User Settings</b>
					</div>
					<br/><br/>
					<table  align=center>
						<tr>
						<td>Setpoint 2</td>
						<td width=10%></td>
						<td >{temp :degF}</td><!--Dynamic Content-->
						</tr>
						<tr>
						<td>Setpoint 3</td>
						<td width=10%></td>
						<td>{temp :degF}</td><!--Dynamic Content-->
						</tr>
						
					</table>
					<hr/>
					<table border=0 align=center>
						<tr>
						<td>Parameter X</td>
						<td width=10%></td>
						<td>{temp :degF}</td><!--Dynamic Content-->
						</tr>
						<tr>
						<td>Parameter Y</td>
						<td width=10%></td>
						<td>{tempe :degF}</td><!--Dynamic Content-->
						</tr>
						<tr>
						<td>Parameter Z</td>
						<td width=10%></td>
						<td>{temp :degF}</td><!--Dynamic Content-->
						</tr>
					</table>
					<br/>
					<input id="buttonid" type="submit"  data-label="1_2" value="Edit"/>
					<button class="btn btn-small btn-inverse" type="button" style="padding:0 10px;">Update</button>
					<br/>				
					<br/>	
					<br/>
					<br/>
					
									
				</div>
			</div>
		</div>
	</div>
	
	
	<div class="container-fluid" style="background-color:#3D7073;">
		<div class="container jumbotron" style="background-color:#3D7073; margin-bottom:0; padding-bottom:0; padding-top:5px;">
			<div class="row" >
				<div class="col-md-3" style="text-align:center; color:#FFFFFF; background-color:#1E3839;">
					<div style="width:100%; background-color:#1E3839; padding:10px 10px;" >
					
					</div>
				</div>
				<div class="col-md-6" style="text-align:center; color:#FFFFFF;">
				
					<div style="width:100%; background-color:#1E3839; padding:10px 10px;" >
					
					</div>
					
				</div>
				<div class="hidden-xs">
				<div class="col-md-3" style="text-align:center; color:#FFFFFF; text-align:center; color:#FFFFFF; background-color:#1E3839;  padding:0 0;">
					<div style="width:100%; background-color:#FF6600; padding:10px 10px;" >
					<b>ModBus Communication</b>
					</div>
				</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="container-fluid" style="background-color:#FFFFFF;">
		<div class="container jumbotron" style="background-color:#FFFFFF; margin-bottom:0; padding-bottom:0; padding-top:5px;">
			<div class="row" >
				<div class="col-md-4" style="text-align:center; color:#00ff99; ">
					<h5>MANUAL OVERRIDE</h5>
						<div class="example-plot" id="chart1"></div><div class="example-plot" id="chart2"></div><div class="example-plot" id="chart3"></div><div class="example-plot" id="chart4"></div>  
<!-- Example scripts go here -->

    <style type="text/css">
      .jqplot-point-label {white-space: nowrap;}
/*    .jqplot-yaxis-label {font-size: 14pt;}*/
/*    .jqplot-yaxis-tick {font-size: 7pt;}*/

    div.jqplot-target {
        height: 400px;
        width: 750px;
        margin: 70px;
    }
    </style>
				</div>
				<div class="col-md-3" style="text-align:center; color:#00ff99;">
				<h5>STORAGE</h5>
						<div id="myStat2" data-dimension="100" data-text="15%" data-info="Storage" data-width="20" data-fontsize="13" data-percent="15" data-fgcolor="#61a9dc" data-bgcolor="#eee" style="float:left;"></div>
						<div id="myStat3" data-dimension="100" data-text="55%" data-info="Database" data-width="20" data-fontsize="13" data-percent="55" data-fgcolor="#61a9dc" data-bgcolor="#eee" style="float:right;"></div>
				</div>
				<div class="col-md-1" style="text-align:center; color:#00ff99;">
				</div>
				<div class="col-md-4" style="text-align:center; color:#00ff99; ">
					<h5>MANUAL OVERRIDE</h5>
						
				</div>
			</div>
		</div>
	</div>
	<hr/>

  </body>
</html>