
To clear the table exept for the last row : 

1. Delete everything but one : 

DELETE FROM mainTable
WHERE LID NOT IN (
 SELECT LID
 FROM (
  SELECT LID
  FROM mainTable
  ORDER BY LID DESC
  LIMIT 1
 ) foo
);


2. Change the LID of the remaining row to 1 : 

UPDATE mainTable SET LID=1;


3. Reset the AUTO_INCREMENT to 1: 

ALTER TABLE mainTable AUTO_INCREMENT=1


To clear the whole table : 
" TRUNCATE TABLE mainTable " is most efficient, as it drops and re-creates the table, which is faster than deleting all rows, 
and it also resets the AUTO_INCREMENT in the process.