<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!--<meta http-equiv="refresh" content=5>-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="images/Icons/Logo.png">
    <title>Chronos : Home Page</title>
	
    <!-- Bootstrap -->
  	<script src="amcharts/amcharts.js" type="text/javascript"></script>
	<script src="amcharts/serial.js" type="text/javascript"></script>	
	<script src="http://code.jquery.com/jquery-latest.js" type="text/javascript"></script>
	<script type="text/javascript">
		setInterval("my_function();",2000);
		function my_function()
		{
			$("#updateSetpoint").load("index.php #inside");
		}
		setInterval("my_functionImage();",2000);
		function my_functionImage()
		{
			$("#updateImage").load("index.php #insideImage");
		}
		setInterval("my_functionInterface();",2000);
		function my_functionInterface()
		{
			$("#updateInterface").load("index.php #insideInterface");
		}
	</script>	
	<script type="text/javascript">
            var chart;
            var chartData = [];
            var chartCursor;
            AmCharts.ready(function () {
                // generate some data first
                generateChartData1();

                // SERIAL CHART
                chart = new AmCharts.AmSerialChart();
                chart.pathToImages = "amcharts/images/";
                chart.dataProvider = chartData;
                chart.categoryField = "date";
                chart.balloon.bulletSize = 5;

                // listen for "dataUpdated" event (fired when chart is rendered) and call zoomChart method when it happens
                chart.addListener("dataUpdated", zoomChart);

                // AXES
                // category
                var categoryAxis = chart.categoryAxis;
                categoryAxis.parseDates = true; // as our data is date-based, we set parseDates to true
                categoryAxis.minPeriod = "mm"; // our data is daily, so we set minPeriod to DD
                categoryAxis.dashLength = 1;
                categoryAxis.minorGridEnabled = true;
                categoryAxis.twoLineMode = true;
                categoryAxis.dateFormats = [{
                    period: 'fff',
                    format: 'JJ:NN:SS'
                }, {
                    period: 'ss',
                    format: 'JJ:NN:SS'
                }, {
                    period: 'mm',
                    format: 'JJ:NN'
                }, {
                    period: 'hh',
                    format: 'JJ:NN'
                }, {
                    period: 'DD',
                    format: 'DD'
                }, {
                    period: 'WW',
                    format: 'DD'
                }, {
                    period: 'MM',
                    format: 'MMM'
                }, {
                    period: 'YYYY',
                    format: 'YYYY'
                }];

                categoryAxis.axisColor = "#DADADA";

                // value
                var valueAxis = new AmCharts.ValueAxis();
                valueAxis.axisAlpha = 0;
                valueAxis.dashLength = 1;
                chart.addValueAxis(valueAxis);

                // GRAPH
                var graph = new AmCharts.AmGraph();
                graph.title = "red line";
                graph.valueField = "visits";
                graph.bullet = "round";
                graph.bulletBorderColor = "#FFFFFF";
                graph.bulletBorderThickness = 2;
                graph.bulletBorderAlpha = 1;
                graph.lineThickness = 2;
                graph.lineColor = "#5fb503";
                graph.negativeLineColor = "#efcc26";
                graph.hideBulletsCount = 50; // this makes the chart to hide bullets when there are more than 50 series in selection
                chart.addGraph(graph);

                // CURSOR
                chartCursor = new AmCharts.ChartCursor();
                chartCursor.cursorPosition = "mouse";
                chartCursor.pan = true; // set it to fals if you want the cursor to work in "select" mode
                chart.addChartCursor(chartCursor);

                // SCROLLBAR
                var chartScrollbar = new AmCharts.ChartScrollbar();
                chart.addChartScrollbar(chartScrollbar);

                chart.creditsPosition = "bottom-right";

                // WRITE
                chart.write("chartdiv");
            });

            // generate some random data, quite different range
            function generateChartData1() {
                 var firstDate =  new Date();
                firstDate.setDate(firstDate.getDate() - 7);
                
	
               
                    // we create date objects here. In your data, you can have date strings
                    // and then set format of your dates using chart.dataDateFormat property,
                    // however when possible, use date objects, as this will speed up chart rendering.
                   <?php
						include('SetConnect.php');
						$sql="SELECT logdatetime from mainTable order by LID desc limit 7";
						$result=mysql_query($sql,$con);
						if($result){
							while($row=mysql_fetch_array($result)){
							$arrDate[] = $row['logdatetime']; 
							}
						}
					?>;
		    
			<?php
						include('SetConnect.php');
						$sql="SELECT returnTemp from mainTable order by LID desc limit 7";
						$result=mysql_query($sql,$con);
						if($result){
							while($row=mysql_fetch_array($result)){
							$arr[] = $row['returnTemp']; 
							}
						}
					?>;
			
                    var visits = [<?php echo $arr[0];?>,<?php echo $arr[1];?>,<?php echo $arr[2];?>,<?php echo $arr[3];?>,<?php echo $arr[4];?>,<?php echo $arr[5];?>,<?php echo $arr[6];?>];
			<!--var newDate = [<?php echo $arrDate[0];?>,<?php echo $arrDate[1];?>,<?php echo $arrDate[2];?>,<?php echo $arrDate[3];?>,<?php echo $arrDate[4];?>,<?php echo $arrDate[5];?>,<?php echo $arrDate[6];?>];	-->
		   <!-- var newDate =  [<?php echo substr($arrDate[0],-8,-6);?>,<?php echo substr($arrDate[1],-6,-1);;?>,<?php substr($arrDate[2],-6,-1);;?>,<?php substr($arrDate[3],-6,-1);?>,<?php echo substr($arrDate[4],-6,-1);?>,<?php echo substr($arrDate[5],-6,-1);?>,<?php substr($arrDate[6],-6,-1);?>];-->
		    for(i=0;i<7;i++){
			var newDate = new Date(firstDate);
                    newDate.setDate(newDate.getDate() + i);
                    chartData.push({
                        date: newDate,
                        visits: visits[i]
		
                    });
		
                }
            }

            // this method is called when chart is first inited as we listen for "dataUpdated" event
            function zoomChart() {
                // different zoom methods can be used - zoomToIndexes, zoomToDates, zoomToCategoryValues
                chart.zoomToIndexes(chartData.length - 40, chartData.length - 1);
            }

        </script>
		

	<script type="text/javascript">
            var chartOne;
            var chartDataOne = [];
            var chartCursorOne;
            AmCharts.ready(function () {
                // generate some data first
                generateChartData2();

                // SERIAL CHART
                chartOne = new AmCharts.AmSerialChart();
                chartOne.pathToImages = "amcharts/images/";
                chartOne.dataProvider = chartDataOne;
                chartOne.categoryField = "date";
                chartOne.balloon.bulletSize = 5;

                // listen for "dataUpdated" event (fired when chart is rendered) and call zoomChart method when it happens
                chartOne.addListener("dataUpdated", zoomChart1);

                // AXES
                // category
                var categoryAxis = chartOne.categoryAxis;
                categoryAxis.parseDates = true; // as our data is date-based, we set parseDates to true
                categoryAxis.minPeriod = "mm"; // our data is daily, so we set minPeriod to DD
                categoryAxis.dashLength = 1;
                categoryAxis.minorGridEnabled = true;
                categoryAxis.twoLineMode = true;
                categoryAxis.dateFormats = [{
                    period: 'fff',
                    format: 'JJ:NN:SS'
                }, {
                    period: 'ss',
                    format: 'JJ:NN:SS'
                }, {
                    period: 'mm',
                    format: 'JJ:NN'
                }, {
                    period: 'hh',
                    format: 'JJ:NN'
                }, {
                    period: 'DD',
                    format: 'DD'
                }, {
                    period: 'WW',
                    format: 'DD'
                }, {
                    period: 'MM',
                    format: 'MMM'
                }, {
                    period: 'YYYY',
                    format: 'YYYY'
                }];

                categoryAxis.axisColor = "#DADADA";

                // value
                var valueAxis = new AmCharts.ValueAxis();
                valueAxis.axisAlpha = 0;
                valueAxis.dashLength = 1;
                chartOne.addValueAxis(valueAxis);

                // GRAPH
                var graph = new AmCharts.AmGraph();
                graph.title = "red line";
                graph.valueField = "visits";
                graph.bullet = "round";
                graph.bulletBorderColor = "#FFFFFF";
                graph.bulletBorderThickness = 2;
                graph.bulletBorderAlpha = 1;
                graph.lineThickness = 2;
                graph.lineColor = "#5fb503";
                graph.negativeLineColor = "#efcc26";
                graph.hideBulletsCount = 50; // this makes the chart to hide bullets when there are more than 50 series in selection
                chartOne.addGraph(graph);

                // CURSOR
                chartCursorOne = new AmCharts.ChartCursor();
                chartCursorOne.cursorPosition = "mouse";
                chartCursorOne.pan = true; // set it to fals if you want the cursor to work in "select" mode
                chartOne.addChartCursor(chartCursorOne);

                // SCROLLBAR
                var chartScrollbar = new AmCharts.ChartScrollbar();
                chartOne.addChartScrollbar(chartScrollbar);

                chartOne.creditsPosition = "bottom-right";

                // WRITE
                chartOne.write("chartdiv1");
            });

            // generate some random data, quite different range
            function generateChartData2() {
                 var firstDate =  new Date();
                firstDate.setDate(firstDate.getDate() - 7);
                

               
                    // we create date objects here. In your data, you can have date strings
                    // and then set format of your dates using chart.dataDateFormat property,
                    // however when possible, use date objects, as this will speed up chart rendering.
                  

                   <?php
						include('SetConnect.php');
						$sql="SELECT waterOutTemp from mainTable order by logdatetime desc limit 7";
						$result=mysql_query($sql,$con);
						if($result){
							while($row=mysql_fetch_array($result)){
							$arrW[] = $row['waterOutTemp']; 
							}
						}
					?>;
			var visits = [<?php echo $arrW[0];?>,<?php echo $arrW[1];?>,<?php echo $arrW[2];?>,<?php echo $arrW[3];?>,<?php echo $arrW[4];?>,<?php echo $arrW[5];?>,<?php echo $arrW[6];?>];
			for(i=0;i<7;i++){
			var newDate = new Date(firstDate);
                    newDate.setDate(newDate.getDate() + i);
                    chartDataOne.push({
                        date: newDate,
                        visits: visits[i]
		
                    });
		}
                
            }

            // this method is called when chart is first inited as we listen for "dataUpdated" event
            function zoomChart1() {
                // different zoom methods can be used - zoomToIndexes, zoomToDates, zoomToCategoryValues
                chartOne.zoomToIndexes(chartDataOne.length - 40, chartDataOne.length - 1);
            }

        </script>
 
	<!--Water Outlet-->

	<script type="text/javascript">
		
	</script>




	<link href="css/style.css" rel="stylesheet">
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/graph.css" rel="stylesheet">
	<style>
		#canvas .circle {
			display: inline-block;
			margin: 1em;
		}

		.circles-decimals {
			font-size: .4em;
		}
		    .btn-file {
    
    height:30px;
    position: relative;
    overflow: hidden;
    }
    .btn-file input[type=file] {
    position: absolute;
    top: 0;
    right: 0;
    min-width: 100%;
    min-height: 100%;
    text-align: right;
    filter: alpha(opacity=0);
    opacity: 0;
    outline: none;
    background: white;
    cursor: inherit;
    display: block;
    }

	
	</style>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body style="font-family:segoe ui;">
  
	
	<!-- Top Container -->
	
	<div class="container-fluid" style="background-color:#3D7073;">
		<div class="container jumbotron" style="background-color:#3D7073; padding-top:0; padding-bottom:2px; margin-bottom:0; ">
			<div class="row" >
				<div class="col-md-3" style="text-align:center; color:#FFFFFF; background-color:#1E3839; padding:10px 10px;">
					<h4><b>CHRONOS</b></h4>
					<h5>SYSTEM-ONLINE</h5><!--Dynamic Content-->
				</div>
				<div class="col-md-6" style="text-align:center; color:#FFFFFF;">
					<h6><a href="#" style="color:#FFFFFF;">About</a> | 
					<a href="#" style="color:#FFFFFF;">Developer Website</a> |
					<a href="#" style="color:#FFFFFF;">Help</a></h6>
					
						<div class="col-md-12"  style="background-color:#1E3839; min-height:50px; padding-top:5px; padding-bottom:5px;">
							<h5>SYSTEM MAP</h5>
						</div>
					
				</div>
				<div class="hidden-xs">
				<div class="col-md-3" style="text-align:center; color:#FFFFFF; text-align:center; color:#FFFFFF; background-color:#1E3839; min-height:84px;">
				<br/>
					<div style="min-width:60px; font-size:10px; color:#FFFFFF; float:left; text-align=center;">
						&nbsp;
						</div>
						<div style="min-width:80px; font-size:10px; color:#FFFFFF; float:left; text-align=center;">
						<img src="<?php
						include('SetConnect.php');
						$sql="SELECT mode from mainTable order by LID desc limit 1";
						$result=mysql_query($sql,$con);
						if($result){
							while($row=mysql_fetch_array($result)){
							if($row['mode']==0)
								echo "images/Icons/WinterSummer/WOn.png";
							else if($row['mode']==1)
								echo "images/Icons/WinterSummer/WOff.png";
							
							}
						}
					?>" /><br/><a href="updateModeOn.php" font-size:10px;">Winter</a>
						</div>
					
					<div style=" min-width:80px; font-size:10px; color:#FFFFFF; float:left; text-align=center;">
						<img src="<?php
						include('SetConnect.php');
						$sql="SELECT mode from mainTable order by LID desc limit 1";
						$result=mysql_query($sql,$con);
						if($result){
							while($row=mysql_fetch_array($result)){
							if($row['mode']==0)
								echo "images/Icons/WinterSummer/SOff.png";
							else if($row['mode']==1)
								echo "images/Icons/WinterSummer/SOn.png";
							
							}
						}
					?>" /><br/><a href="updateModeOff.php" style="color:#FFFFFF; font-size:10px;">Summer</a>
					</div>

					
				</div>
				</div>
			</div>
		</div>
	</div>
	<!-- End of Top Container -->
	
	<div class="container-fluid"  id="updateSetpoint" style="background-color:#3D7073;" >
		<div class="container jumbotron" id="inside" style="background-color:#3D7073; padding-bottom:0; padding-top:2px;">
			<div class="row" >
				<div class="col-md-3"  style="text-align:center; color:#FFFFFF; background-color:#224042; padding:0 0; min-height:358px;">
					<div style="width:100%; background-color:#FF6600; padding:10px 10px;" >
					<b>System Statistics</b>
					</div>
					<h5>Outside Temperature</h5>
					<h6 id="OutsideTemp"><?php
						include('SetConnect.php');
						$sql="SELECT outsideTemp from mainTable order by LID desc limit 1";
						$result=mysql_query($sql,$con);
						if($result){
							while($row=mysql_fetch_array($result)){
							echo $row['outsideTemp']; 
							}
						}
					?> &deg;F</h6>		<!--Dynamic Content-->
					<h6 ><small style="color:#FFFFFF;">wx.thomaslivestock.com</small></h6>
					<table  align=center>
						<tr>
						<td >Water Inlet</td>
						<td width=5% style="border-right:1px solid rgba(255, 255, 255, 0.5);"></td>
						<td>&nbsp;&nbsp;&nbsp;Water Outlet</td><!--Dynamic Content-->
						</tr>
						<tr>
						<td><?php
						include('SetConnect.php');
						$sql="SELECT returnTemp from mainTable order by LID desc limit 1";
						$result=mysql_query($sql,$con);
						if($result){
							while($row=mysql_fetch_array($result)){
							echo $row['returnTemp']; 
							}
						}
					?> &deg;F</td>
						<td width=5% style="border-right:1px solid rgba(255, 255, 255, 0.5);"></td>
						<td><?php
						include('SetConnect.php');
						$sql="SELECT waterOutTemp from mainTable order by LID desc limit 1";
						$result=mysql_query($sql,$con);
						if($result){
							while($row=mysql_fetch_array($result)){
							echo $row['waterOutTemp']; 
							}
						}
					?> &deg;F</td><!--Dynamic Content-->
						</tr>
						
					</table>
					<hr/ style="opacity:0.5;">
					<table align=center>
						<h6>Activity Stream</h6>
						<tr>
						<td></td>
						<td width=5% style="border-left:1px solid rgba(255, 255, 255, 0.5);"></td>
						<td>&nbsp;&nbsp;&nbsp;Date &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Time</td><!--Dynamic Content-->
						<td width=5% style="border-right:1px solid rgba(255, 255, 255, 0.5);"></td>
						<td>&nbsp;&nbsp;&nbsp;Status</td>
						</tr>
						<tr>
						<td>Boiler</td>
						<td width=5% style="border-left:1px solid rgba(255, 255, 255, 0.5);"></td>
						<td><?php
						include('SetConnect.php');
						$sql="SELECT timeStamp from actStream where TID=1";
						$result=mysql_query($sql,$con);
						if($result){
							while($row=mysql_fetch_array($result)){
							echo $row['timeStamp']; 
							}
						}
					?></td><!--Dynamic Content-->
						<td width=5% style="border-right:1px solid rgba(255, 255, 255, 0.5);"></td>
						<td><?php
						include('SetConnect.php');
						$sql="SELECT status from actStream where TID=1";
						$result=mysql_query($sql,$con);
						if($result){
							while($row=mysql_fetch_array($result)){
							if ($row[status]==1)
								echo "ON"; 
							else if ($row[status]==0)
								echo "OFF";
							}
						}
					?></td>
						</tr>
						<tr>
						<td>Chiller</td>
						<td width=5% style="border-left:1px solid rgba(255, 255, 255, 0.5);"></td>
						<td><?php
						include('SetConnect.php');
						$sql="SELECT timeStamp from actStream where TID=2";
						$result=mysql_query($sql,$con);
						if($result){
							while($row=mysql_fetch_array($result)){
							echo $row['timeStamp']; 
							}
						}
					?></td><!--Dynamic Content-->
						<td width=5% style="border-right:1px solid rgba(255, 255, 255, 0.5);"></td>
						<td><?php
						include('SetConnect.php');
						$sql="SELECT status from actStream where TID=2";
						$result=mysql_query($sql,$con);
						if($result){
							while($row=mysql_fetch_array($result)){
							if ($row[status]==1)
								echo "ON"; 
							else if ($row[status]==0)
								echo "OFF";
							}
						}
					?></td>
						</tr>
						<tr>
						<td>Chiller 2&nbsp;&nbsp;</td>
						<td width=5% style="border-left:1px solid rgba(255, 255, 255, 0.5);"></td>
						<td><?php
						include('SetConnect.php');
						$sql="SELECT timeStamp from actStream where TID=3";
						$result=mysql_query($sql,$con);
						if($result){
							while($row=mysql_fetch_array($result)){
							echo $row['timeStamp']; 
							}
						}
					?></td><!--Dynamic Content-->
						<td width=5% style="border-right:1px solid rgba(255, 255, 255, 0.5);"></td>
						<td><?php
						include('SetConnect.php');
						$sql="SELECT status from actStream where TID=3";
						$result=mysql_query($sql,$con);
						if($result){
							while($row=mysql_fetch_array($result)){
							if ($row[status]==1)
								echo "ON"; 
							else if ($row[status]==0)
								echo "OFF";
							}
						}
					?></td>
						</tr>
					</table>
					
					
				</div>
				
				
				<div class="col-md-6" id="updateImage" style="text-align:center; color:#FFFFFF; min-height:300px;">
					<div  id="insideImage" style="width:100%; padding:10px 10px; background-image:url('<?php
						include('SetConnect.php');
						$sql="SELECT returnTemp from mainTable order by logdatetime desc limit 1";
						$sql2="SELECT waterOutTemp from mainTable order by LID desc limit 1";
						$result=mysql_query($sql,$con);
						$result1=mysql_query($sql2,$con);
						if($result){
							while(($row=mysql_fetch_array($result)) && ($row1=mysql_fetch_array($result1))){
							if ($row[returnTemp]>60&&$row1[waterOutTemp]>60)
								echo "images/Icons/MainImage/OHIH.png"; 
							else if ($row[returnTemp]<60&&$row1[waterOutTemp]<60)
								echo "images/Icons/MainImage/OCIC.png"; 
							else if ($row[returnTemp]<60&&$row1[waterOutTemp]>60)
								echo "images/Icons/MainImage/OHIC.png"; 
							else if ($row[returnTemp]>60&&$row1[waterOutTemp]<60)
								echo "images/Icons/MainImage/OCIH.png";
							}
						}
					?>'); font-size:11px; background-repeat: no-repeat; min-height:300px;" >
						<table border="0" >
							<tr height=30px>
								<td width="55%"></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
							<tr>
							<tr>
								<td ></td>
								<td width=11%></td>	
								<td></td>
								<td></td>
								<td align="right"></td>
							<tr>
							<tr>
								<td></td>
								<td></td>
								<td align="left"><div><br/><img src="<?php
						include('SetConnect.php');
						$sql="SELECT status from actStream where TID=1";
						$result=mysql_query($sql,$con);
						if($result){
							while($row=mysql_fetch_array($result)){
							if ($row[status]==1)
								echo "images/Icons/Boiler/Boiler-ON.png"; 
							else if ($row[status]==0)
								echo "images/Icons/Boiler/Boiler-OFF.png"; 
							
							}
						}
					?>" /></div></td>
								<td></td>
								<td></td>
							<tr>
							<tr>
								<td></td>
								<td></td>	
								<td align="left">&nbsp;Boiler 1</td>
								<td></td>
								<td></td>
							<tr>
							<tr>
								<td></td>
								<td></td>
								<td align="left" width=100px><br/><img src="<?php
						include('SetConnect.php');
						$sql="SELECT status from actStream where TID=2";
						$result=mysql_query($sql,$con);
						if($result){
							while($row=mysql_fetch_array($result)){
							if ($row[status]==1)
								echo "images/Icons/Boiler/Chiller-ON.png"; 
							else if ($row[status]==0)
								echo "images/Icons/Boiler/Chiller-OFF.png"; 
							
							}
						}
					?>" /></td>
								<td align="left"><br/><img src="<?php
						include('SetConnect.php');
						$sql="SELECT status from actStream where TID=3";
						$result=mysql_query($sql,$con);
						if($result){
							while($row=mysql_fetch_array($result)){
							if ($row[status]==1)
								echo "images/Icons/Boiler/Chiller-ON.png"; 
							else if ($row[status]==0)
								echo "images/Icons/Boiler/Chiller-OFF.png"; 
							
							}
						}
					?>"/></td>
								<td></td>
							<tr>
							<tr>
								<td></td>
								<td></td>
								<td align="left">Chiller 1</td>
								<td align="left">Chiller 2</td>
								<td></td>
							<tr>
						</table>
					</div>
					
				</div>
				
				<div class="col-md-3" style="text-align:center; color:#FFFFFF; background-color:#224042; padding:0 0; min-height:350px;">
					<div style="width:100%; background-color:#FF6600; padding:10px 10px;" >
					<b>User Settings</b>
					</div>
					<br/><br/>
					<form name="UpdateParameter" action="UpdatePara.php" method=post>

					<table  align=center>
						<tr>
						<td>Setpoint 1</td>
						<td width=10%></td>
						<td><?php
						include('SetConnect.php');
						$sql="SELECT setPoint1 from mainTable order by LID desc limit 1";
						$result=mysql_query($sql,$con);
						if($result){
							while($row=mysql_fetch_array($result)){
							echo $row['setPoint1']; 
							}
						}
					?> &deg;F</td><!--Dynamic Content-->
						<td ></td>
						<td style="color:#000000;"><input type="text" name="setpoint1" size=2></td>
						<td width=3%></td>
						<td><input type="Submit" value="Update" style="color:#000000;"></td>
						</tr>
						<tr>
						<td>Setpoint 2</td>
						<td width=10%></td>
						<td><?php
						include('SetConnect.php');
						$sql="SELECT setPoint2 from mainTable order by LID desc limit 1";
						$result=mysql_query($sql,$con);
						if($result){
							while($row=mysql_fetch_array($result)){
							echo $row['setPoint2']; 
							}
						}
					?> &deg;F</td><!--Dynamic Content--><td >&nbsp;&nbsp;&nbsp;</td>
						<td style="color:#000000;"><input type="text" name="setpoint2" size=2></td>
						<td width=3%></td>
						<td><input type="Submit" value="Update" style="color:#000000;"></td>
						</tr>
						
					</table>
					<hr/ style="opacity:0.5;">
					<table border=0 align=center>
						<tr>
						<td>Parameter X</td>
						<td width=10%></td>
						<td><?php
						include('SetConnect.php');
						$sql="SELECT parameterX from mainTable order by LID desc limit 1";
						$result=mysql_query($sql,$con);
						if($result){
							while($row=mysql_fetch_array($result)){
							echo $row['parameterX']; 
							}
						}
					?> &deg;F</td><!--Dynamic Content--><td ></td>
						<td style="color:#000000;"><input type="text" name="parameterX" size=2></td>
						<td width=3%></td>
						<td><input type="Submit" value="Update" style="color:#000000;"></td>
						</tr>
						<tr>
						<td>Parameter Y</td>
						<td width=10%></td>
						<td><?php
						include('SetConnect.php');
						$sql="SELECT parameterY from mainTable order by LID desc limit 1";
						$result=mysql_query($sql,$con);
						if($result){
							while($row=mysql_fetch_array($result)){
							echo $row['parameterY']; 
							}
						}
					?> &deg;F</td><td ></td>
						<td style="color:#000000;"><input type="text" name="parameterY" size=2></td><!--Dynamic Content-->
						<td width=3%></td>
						<td><input type="Submit" value="Update" style="color:#000000;"></td>
						</tr>
						<tr>
						<td>Parameter Z</td>
						<td width=10%></td>
						<td><?php
						include('SetConnect.php');
						$sql="SELECT parameterZ from mainTable order by LID desc limit 1";
						$result=mysql_query($sql,$con);
						if($result){
							while($row=mysql_fetch_array($result)){
							echo $row['parameterZ']; 
							}
						}
					?> &deg;F</td><!--Dynamic Content--><td >&nbsp;&nbsp;</td>
						<td style="color:#000000;"><input type="text" name="parameterZ" size=2></td>
						<td width=3%></td>
						<td><input type="Submit" value="Update" style="color:#000000;"></td>
						</tr>
					</table>
					<br/>
					<!-- input type="Submit" value="Update" style="color:#000000;" -->
					<br/>				
					<br/>	
					<br/>
					<br/>
					</form>
									
				</div>
			</div>
		</div>
	</div>
	
	
	<div class="container-fluid" style="background-color:#3D7073;">
		<div class="container jumbotron" style="background-color:#3D7073; margin-bottom:0; padding-bottom:0; padding-top:5px;">
			<div class="row" >
				<div class="col-md-3" style="text-align:center; color:#FFFFFF; background-color:#1E3839;">
					<div style="width:100%; background-color:#1E3839; padding:20px 20px;" >
					
					</div>
				</div>
				<div class="col-md-6" style="text-align:center; color:#FFFFFF;">
				
					<div style="width:100%; background-color:#1E3839; padding:8px 8px;" >
						<span class="button"><a href="updateReboot.php"  style="color:#000000; font-size:12px">Restart</a></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="button"><a href="updateShutdown.php" style="color:#000000; font-size:12px">Shutdown</a></span>
					</div>
					
				</div>
				<div class="hidden-xs">
				<div class="col-md-3" style="text-align:center; color:#FFFFFF; text-align:center; color:#FFFFFF; background-color:#1E3839;  padding:0 0;">
					
					<div style="width:100%; background-color:#1E3839; padding:20px 20px;" >
					
					</div>
					<!--<div style="width:100%; background-color:#FF6600; padding:10px 10px;" >
					<b>ModBus Communication</b>
					</div>
					
					<h5></h5>
					<h6 id="OutsideTemp">OFFLINE</h6>	-->	<!--Dynamic Content-->
					
				</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="container-fluid" id="updateInterface" style="background-color:#FFFFFF;">
		<div class="container jumbotron" id="insideInterface" style="background-color:#FFFFFF; margin-bottom:0; padding-bottom:0; padding-top:5px;">
			<div class="row" >
				<div class="col-md-4" style="width: 150px; text-align:center; color:#3D7073;">
				</div>
				<div class="col-md-4" style="text-align:center; color:#3D7073; ">
					<h5>MANUAL OVERRIDE</h5><br/>
					<div style="min-height:100px; min-width:35px; float:left;">
							
						</div>
						<div style="background-image:url('<?php
						include('SetConnect.php');
						$sql="SELECT MO_B from mainTable order by LID desc limit 1";
						$result=mysql_query($sql,$con);
						if($result){
							while($row=mysql_fetch_array($result)){
							if($row['MO_B']==0)
								echo "images/Icons/Manual/Auto.png";
							else if($row['MO_B']==1)
								echo "images/Icons/Manual/ON.png";
							else if($row['MO_B']==2)
								echo "images/Icons/Manual/OFF.png";
							}
						}
					?>'); 	background-repeat: no-repeat; min-height:100px; min-width:110px; font-size:10px; color:#FFFFFF; float:left;">
							<table border=0>
								<tr height=10%>
								<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
								</tr>
								<tr>
								<tr height=15px>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								</tr>
								<tr>
								<td></td>
								<td></td>
								<td><a href="UpdateAutoB.php">&nbsp;Auto</a></td>
								<td></td>
								<td><a href="UpdateOnB.php">On</a></td>
								<td></td>
								</tr>
								<tr >
								<td>&nbsp;</td>
								<td></td>
								<td></td>
								<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
								<td></td>
								<td></td>
								</tr>
								<tr>
								<td></td>
								<td></td>
								<td></td>
								<td><a href="UpdateOffB.php">Off</a></td>
								<td></td>
								<td></td>
								</tr>
								<tr height=5%>
								<td colspan=6>&nbsp;</td>
								</tr>
								<tr height=5%>
								<td colspan=6>&nbsp;</td>
								</tr>
								<tr>
								<td colspan=6 style="color:#3D7073; font-size:12px;">&nbsp;&nbsp;&nbsp;Boiler</td>
								</tr>

							</table>
							
							
						</div>
						<div style="background-image:url('<?php
						include('SetConnect.php');
						$sql="SELECT MO_C1 from mainTable order by LID desc limit 1";
						$result=mysql_query($sql,$con);
						if($result){
							while($row=mysql_fetch_array($result)){
							if($row['MO_C1']==0)
								echo "images/Icons/Manual/Auto.png";
							else if($row['MO_C1']==1)
								echo "images/Icons/Manual/ON.png";
							else if($row['MO_C1']==2)
								echo "images/Icons/Manual/OFF.png";
							}
						}
					?>'); 	background-repeat: no-repeat; min-height:100px; min-width:110px; font-size:10px; color:#FFFFFF; float:left;">
							<table border=0>
								<tr height=10%>
								<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
								</tr>
								<tr>
								<tr height=15px>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								</tr>
								<tr>
								<td></td>
								<td></td>
								<td><a href="UpdateAutoC1.php">&nbsp;Auto</a></td>
								<td></td>
								<td><a href="UpdateOnC1.php">On</a></td>
								<td></td>
								</tr>
								<tr >
								<td>&nbsp;</td>
								<td></td>
								<td></td>
								<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
								<td></td>
								<td></td>
								</tr>
								<tr>
								<td></td>
								<td></td>
								<td></td>
								<td><a href="UpdateOffC1.php">Off</a></td>
								<td></td>
								<td></td>
								</tr>
								<tr height=5%>
								<td colspan=6>&nbsp;</td>
								</tr>
								<tr height=5%>
								<td colspan=6>&nbsp;</td>
								</tr>
								<tr>
								<td colspan=6 style="color:#3D7073; font-size:12px;">&nbsp;&nbsp;Chiller 1</td>
								</tr>
							</table>
							
							
						</div>
						<div style="background-image:url('<?php
						include('SetConnect.php');
						$sql="SELECT MO_C2 from mainTable order by LID desc limit 1";
						$result=mysql_query($sql,$con);
						if($result){
							while($row=mysql_fetch_array($result)){
							if($row['MO_C2']==0)
								echo "images/Icons/Manual/Auto.png";
							else if($row['MO_C2']==1)
								echo "images/Icons/Manual/ON.png";
							else if($row['MO_C2']==2)
								echo "images/Icons/Manual/OFF.png";
							}
						}
					?>'); 	background-repeat: no-repeat; min-height:100px; min-width:110px; font-size:10px; color:#FFFFFF; float:left;">
							<table border=0>
								<tr height=10%>
								<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
								</tr>
								<tr>
								<tr height=15px>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								</tr>
								<tr>
								<td></td>
								<td></td>
								<td><a href="UpdateAutoC2.php">&nbsp;Auto</a></td>
								<td></td>
								<td><a href="UpdateOnC2.php">On</a></td>
								<td></td>
								</tr>
								<tr >
								<td>&nbsp;</td>
								<td></td>
								<td></td>
								<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
								<td></td>
								<td></td>
								</tr>
								<tr>
								<td></td>
								<td></td>
								<td></td>
								<td><a href="UpdateOffC2.php">Off</a></td>
								<td></td>
								<td></td>
								</tr>
								<tr height=5%>
								<td colspan=6>&nbsp;</td>
								</tr>
								<tr height=5%>
								<td colspan=6>&nbsp;</td>
								</tr>
								<tr>
								<td colspan=6 style="color:#3D7073; font-size:12px;">&nbsp;&nbsp;Chiller 2</td>
								</tr>
							</table>
							
							
						</div>
						<!--<img src="images/Icons/Manual/Auto.png">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="images/Icons/Manual/On.png">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="images/Icons/Manual/off.png">-->
							
				</div>
				<div class="col-md-4" style="width: 120px; text-align:center; color:#3D7073;">
				<!--<h5>STORAGE</h5>
					<div id="canvas">
						
						<div class="circle" id="circles-1"></div> 
						<div class="circle" id="circles-2"></div><br/>
						<table border=0 style="font-size:12px;">
						<tr>
						<td width=110px></td>
						<td>SD Card</td>
						<td width=80px></td>
						<td>Database</td>
						<tr>	
						</table>
					</div>

	<script src="js/circles.js"></script>
	<script>
		var colors = [
				['#DADADA', '#40B3FF'], ['#FCE6A4', '#EFB917'], ['#BEE3F7', '#45AEEA'], ['#F8F9B6', '#D2D558'], ['#F4BCBF', '#D43A43']
			],
			circles = [];

		for (var i = 1; i <= 2; i++) {
			var child = document.getElementById('circles-' + i),
				 
				
				var percentage[] = <?php
					$myFileTin =fopen("sysStatus.txt","r") or die("Unable to open File");
					$members = array();
					while(!feof($myFileTin)){
						$members[]=fgets($myFileTin);
						
					}
					echo $members[5];	
					fclose($myFileTin);?>,	

				
				
				circle = Circles.create({
					id:         child.id,
					value:      percentage[i],
					radius:     getWidth(),
					width:      12,
					colors:     colors[i - 1]
				});
				
				
			circles.push(circle);
			
			
				
				
		}

		window.onresize = function(e) {
			for (var i = 0; i < circles.length; i++) {
				circles[i].updateRadius(getWidth());
			}
		};

		function getWidth() {
			return window.innerWidth / 30;
		}

	</script>
	-->
				</div>
			
				<div class="col-md-4"  style="text-align:center; color:#3D7073; ">
					<h5>INTERFACE STATUS</h5><br/>
					<img src="<?php
					$myFileTin =fopen("sysStatus.txt","r") or die("Unable to open File");
					$members = array();
					while(!feof($myFileTin)){
						$members[]=fgets($myFileTin);
						
					}
						if($members[0]== 0 && $members[1]==0)
							echo "images/Icons/TINTOUT/TOTO.png";
						else if($members[0]== 1 && $members[1]==1)
							echo "images/Icons/TINTOUT/TFTF.png";
						else if($members[0]== 1 && $members[1]==0)
							echo "images/Icons/TINTOUT/TFTO.png";
						else if($members[0]== 0 && $members[1]==1)
							echo "images/Icons/TINTOUT/TOTF.png";
				
					fclose($myFileTin);
				?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="<?php
					$myFileTin =fopen("sysStatus.txt","r") or die("Unable to open File");
					$members = array();
					while(!feof($myFileTin)){
						$members[]=fgets($myFileTin);
						
					}
						if($members[2]== 0 && $members[3]==0)
							echo "images/Icons/DBWEB/DODBO.png";
						else if($members[2]== 1 && $members[3]==1)
							echo "images/Icons/DBWEB/DFDBF.png";
						else if($members[2]== 1 && $members[3]==0)
							echo "images/Icons/DBWEB/DFDBO.png";
						else if($members[2]== 0 && $members[3]==1)
							echo "images/Icons/DBWEB/DODBF.png";
				
					fclose($myFileTin);
				?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="<?php
					$myFileTin =fopen("sysStatus.txt","r") or die("Unable to open File");
					$members = array();
					while(!feof($myFileTin)){
						$members[]=fgets($myFileTin);
						
					}
						if($members[4]== 0)
							echo "images/Icons/GPIO/GPIOO.png";
						else if($members[4]== 1)
							echo "images/Icons/GPIO/GPIOF.png";
						
				
					fclose($myFileTin);
				?>">
				</div>
			</div>
		</div>
	</div>
	<hr/>
		<div class="container-fluid" style="background-color:#FFFFFF;">
		<div class="container jumbotron" style="background-color:#FFFFFF; margin-bottom:0; padding-bottom:0; padding-top:5px;">
			<div class="row" >
				<div class="col-md-6" style="text-align:center; color:#3D7073; ">
					<h5>Chart - WaterInlet </h5>
<!-- end Graph HTML -->

			 <div id="chartdiv" style="width:100%; height: 400px;"></div>
       
					
    
				</div>
				
				
				<div class="col-md-6" style="text-align:center; color:#3D7073; ">
					<h5>Chart-WaterOutlet </h5>
						 <div id="chartdiv1" style="width:100%; height: 400px;"></div>
        
				</div>
			</div>
		</div>
	</div>
	<hr/>
	<div class="container-fluid" style="background-color:#FFFFFF;">
		<div class="container jumbotron" style="background-color:#FFFFFF; margin-bottom:0; padding-bottom:0; padding-top:5px;">
			<div class="row" >
				<div class="col-md-5" style="text-align:center; color:#FFFFFF; background-color:#3d7073; min-height:100px;">
					<div style="width:100%; background-color:#3d7073; padding:10px 10px; font-size:16px;" >
						Firmware Upgrade | <br/><br/><form action="updateFirmware.php" method="POST" enctype="multipart/form-data">
						<input type="file" name="file" style="width:350px; float:left;">
						 <input type=submit value=Update name=Update style="color:#000000;" class="btn btn-default">
						</form>
					</div>
				</div>
				<div class="col-md-2" style="text-align:center; color:#FFFFFF;">
					
				</div>
				
				<div class="col-md-5" style="text-align:center; color:#FFFFFF; background-color:#3d7073; min-height:100px;">
					<div style="width:100%; background-color:#3d7073; padding:10px 10px; font-size:16px;" >
						 <br/>
						  Download Log  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-default btn-file">
							 <a href="dump.php" style="color:#000000;">View in Excel</a>
							</span>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<hr/>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>


  </body>
</html>